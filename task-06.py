from imports import *

'''

    Task 6

    You are sailing along nicely. However, don't let your guards down. 
    For any payments system, there are bound to be shady people wanting to take advantage. 
    These people are trying to steal money from unwitting victims by stealing 
    their credit card numbers and bank account access. 
    Your help is needed to analyze our payment history and find some patterns for fraudulent payments. 
    We'll provide you a sample of our payments with 10 fraudsters in it that we successfully caught. 
    It's not over yet, though - 100 more payments are coming in, and waiting for your justice! 
    But keep in mind that if you mark a good payment as fraudulent, 
    it will be as damaging as catching a real fraudster.

            "GET /payment/history - get the past payments that have been made by confirmed fraudsters",
            "GET /payment - get payments that should be checked before we process them",
            "PUT /payment/{id}/fraud - to mark a transaction as fraud",
            "DELETE /payment/{id}/fraud - to mark a transaction as non-fraud"

'''


class Detector:
    def __init__(self, history):
        self.ip_masks = set([self.get_mask(payment['ip']) for payment in history if payment['fraud']])

    @staticmethod
    def get_mask(ip):
        return '.'.join(ip.split('.')[:-1])

    def is_fraud(self, payment):
        return self.get_mask(payment['ip']) in self.ip_masks


def process_all():
    def mark_fraud(payment):
        endpoint = '/payment/%s/fraud' % payment['id']
        current_result = twput(endpoint)
        msg = ['fraud', endpoint, current_result]
        print(msg)
        return msg

    def mark_non_fraud(payment):
        endpoint = '/payment/%s/fraud' % payment['id']
        current_result = twdelete(endpoint)
        msg = ['non-fraud', endpoint, current_result]
        print(msg)
        return msg

    def process(payment, detector):
        if detector.is_fraud(payment):
            return mark_fraud(payment)
        else:
            return mark_non_fraud(payment)

    history = twget('task-06-history', 'payment/history', saving=True)
    payments = twget('task-06-payments', 'payment', saving=True)
    detector = Detector(history)

    save('task-06-post', [process(p, detector) for p in payments])


if __name__ == "__main__":
    task_get(6)
    task_start(6)
    process_all()
    task_finish(6)
