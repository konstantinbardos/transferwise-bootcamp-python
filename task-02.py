from imports import *

'''
    Task 2:
    
    Take a second to imagine that you are in a room with 100 chairs arranged in a circle. 
    
    These chairs are numbered sequentially from 1 to 100. 
    At some point in time, the person in chair #1 will be removed from the room along with his chair. 
    The person in chair #2 will be skipped, and the person in chair #3 will be told to leave. 
    
    Next to go is person in chair #6. 
    In other words, 1 person will be skipped initially, and then 2, 3, 4.. and so on. 
    This pattern of skipping will keep going around the circle until there is only one person remaining... 
    You have to figure out which is the number of the last remaining chair. Who survives this Game of Chairs?"

'''


def get_survivor():
    people = [i for i in range(1, 100 + 1)]
    to_skip = 0
    position = 0

    while len(people) > 1:
        position = (position + to_skip) % len(people)
        person = people.pop(position)
        to_skip += 1
        print("Removed: Person %03d - with index %03d - (%d to skip, %s left) - %s" % (
            person, position, to_skip, len(people), people))

    survivor = people.pop()
    print("Survivor: Person: %03d" % survivor)
    return survivor


if __name__ == "__main__":
    task_get(2)
    task_start(2)
    twpost('task-02-post', 'survivor/%d' % get_survivor())
    task_finish(2)
