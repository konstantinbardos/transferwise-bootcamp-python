from imports import *

'''
    Task 3
    
    With this task, we're going to get into the TransferWise story. 
    You will virtually take part in building up the first blocks of how the platform works. 
    We will now be moving customer money without borders. 
    You have opened up different bank accounts all over the world. 
    Each bank account is denominated in a specific currency. 
    We also have customers who have set up payments through TransferWise. 
    Your goal is to pay out this money to the recipient's bank accounts from our own bank accounts. 
    Have fun!
    
'''


def process_all():
    bank_list = twget('task-03-bank', 'bank')
    bank_accounts = twget('task-03-bank_accounts', 'bankAccount')
    payments = twget('task-03-payments', 'payment')
    bank_map = {bank['id']: bank['name'] for bank in bank_list}

    print()

    def get_source_account(currency, amount):
        f = lambda x: \
            x['currency'] == currency and \
            x['accountName'] == 'TransferWise Ltd' and \
            x['balance'] >= amount
        return list(filter(f, bank_accounts)).pop()

    payment_results = []

    for payment in payments:
        source_account = get_source_account(payment['sourceCurrency'], payment['amount'])
        endpoint = 'bank/%s/transfer/%s/%s/%s/%s' % \
                   (quote(bank_map[source_account['bankId']]),
                    source_account['accountNumber'],
                    quote(bank_map[payment['recipientBankId']]),
                    payment['iban'],
                    str(payment['amount']))

        result = twpost('payment', endpoint=endpoint, saving=False)
        print(endpoint, result)
        payment_results.append(result)

    save('task-03-payments', payment_results)


if __name__ == "__main__":
    # task_get(3)
    # task_start(3)
    process_all()
    task_finish(3)
