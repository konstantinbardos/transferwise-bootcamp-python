from imports import *

'''
    Task 5
    
    Now that we have the basics covered, it's time to look at how we can 
    build a system that follows the rules that government regulators 
    have in different countries. For this purpose, we should check 
    if the person receiving the money is somebody who holds power 
    in a political office - also known as a Politically Exposed Person (PEP). 
    You have available a list of the people who should be marked as PEP.
    
        "GET /payment - get payments that should be checked before we process them",
        "PUT /payment/{id}/aml - to mark transaction recipient as 'politically exposed person' recipient",
        "DELETE /payment/{id}/aml - to mark transaction recipient as not 'politically exposed person' recipient"

'''


def process_all():

    peps_raw = task_get(5)['peps']
    payments = twget('task-05-payment', 'payment', saving=True)

    def get_pep(string):
        res = string.split(' - ')
        return res[0], res[1]

    peps = list(map(get_pep, peps_raw))

    def f(elem, name, country):
        pep_name, pep_country = elem
        return pep_name == name and pep_country == country

    def in_peps(payment):
        name = payment['recipientName']
        country = payment['recipientCountry']
        return len(list(filter(lambda e: f(e, name, country), peps))) > 0

    def process(payment):
        endpoint = 'payment/%s/aml' % payment['id']
        if in_peps(payment):
            result = twput(endpoint)
        else:
            result = twdelete(endpoint)
        print(endpoint, str(result))
        return result

    save('task-05-post', [process(payment) for payment in payments])


if __name__ == "__main__":
    task_get(5)
    task_start(5)
    process_all()
    task_finish(5)
