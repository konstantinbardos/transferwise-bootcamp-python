# Transferwise Bootcamp Competition 2017

## Solutions for tasks 1-6 [Python version]


### Task 1 - Puttin' on the Ritz


Alright. Let's start off simple.
Your job is to send us your name.
In the unlikely event you've forgotten your name, you can ask me.
```
"POST /name/<your name>",
"GET /name"
```

---
### Task 2 - Game of Chairs


Take a second to imagine that you are in a room with 100 chairs arranged in a circle.

These chairs are numbered sequentially from 1 to 100.
At some point in time, the person in chair #1 will
be removed from the room along with his chair.
The person in chair #2 will be skipped,
and the person in chair #3 will be told to leave.

Next to go is person in chair #6.
In other words, 1 person will be skipped
initially, and then 2, 3, 4.. and so on.
This pattern of skipping will keep going
around the circle until there is only one person remaining...
You have to figure out which is the number of the last
remaining chair. Who survives this Game of Chairs?"
```
"POST /survivor/{answer}"
```

---
### Task 3 - Bank Network Hell

With this task, we're going to get into the TransferWise story.
You will virtually take part in building up the first blocks of how the platform works.
We will now be moving customer money without borders.
You have opened up different bank accounts all over the world.
Each bank account is denominated in a specific currency.
We also have customers who have set up payments through TransferWise.
Your goal is to pay out this money to the recipient's
bank accounts from our own bank accounts. Have fun!
```
"GET /bank - list all banks",
"GET /bankAccount - list all bank accounts",
"GET /payment - list all payments set up by customers",
"POST /bank/<bank name>/transfer/<source account number>/<target bank name>/<target account number>/<amount>"
```

---
### Task 4 - Cheating's Not Nice

As a next step, let's try to make a few things right in this world.
Let's catch the banks and currency exchange companies pulling dirty tricks on consumers.
Let's calculate by how much they are cheating on you by adding hidden fees.
You have access to a set of quotes offered by different companies.
You also have access to the real mid-market rate.Catch the cheaters.
Calculate how big are the hidden fees.

```
"GET /currency - get all currencies supported by the companies who are making offers.",
"GET /company - get all companies that are making offers",
"GET /quote/<amount>/<sourceCurrency>/<targetCurrency> - get all quotes offered by various banks to transfer a specific amount of money.",
"GET /rate/midMarket/<sourceCurrency>/<targetCurrency> - get the real mid-market rate for a currency pair",
"POST /hiddenFee/forCompany/<companyName>/<sourceAmount>/<sourceCurrency>/<targetCurrency>/<hiddenFeePercentage> - save the percentage that a certain company is cheating their customers on. To calculate hiddenFeePercentage, calculate the difference between what a recipient should get if the company used a fair mid-market rate, and how much more money that would be worth in the source currency. You need to post answers for the amounts of 100, 1000, and 10000 in the source currency. You need to get at least one rate right to move on to the next task. Note that we only care about the number without decimal places (i.e. if the hidden fee is 3%,you should send 3.0)"
```

---
### Task 5 - Politically Correct Code

Now that we have the basics covered, it's time to look at how we can
build a system that follows the rules that government regulators
have in different countries. For this purpose, we should check
if the person receiving the money is somebody who holds power
in a political office - also known as a Politically Exposed Person (PEP).
You have available a list of the people who should be marked as PEP.

```
"GET /payment - get payments that should be checked before we process them",
"PUT /payment/{id}/aml - to mark transaction recipient as 'politically exposed person' recipient",
"DELETE /payment/{id}/aml - to mark transaction recipient as not 'politically exposed person' recipient"
```

---
### Task 6 - Not Everything Is As It Seems

You are sailing along nicely. However, don't let your guards down.
For any payments system, there are bound to be shady people wanting to take advantage.
These people are trying to steal money from unwitting victims by stealing
their credit card numbers and bank account access.
Your help is needed to analyze our payment history and find some patterns for fraudulent payments.
We'll provide you a sample of our payments with 10 fraudsters in it that we successfully caught.
It's not over yet, though - 100 more payments are coming in, and waiting for your justice!
But keep in mind that if you mark a good payment as fraudulent,
it will be as damaging as catching a real fraudster.

```
"GET /payment/history - get the past payments that have been made by confirmed fraudsters",
"GET /payment - get payments that should be checked before we process them",
"PUT /payment/{id}/fraud - to mark a transaction as fraud",
"DELETE /payment/{id}/fraud - to mark a transaction as non-fraud"
```