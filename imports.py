import json
import requests
from urllib.parse import quote_plus, quote

url = 'http://54.229.242.6/'
params = dict(token='36db9a2e40f3f1754fa7baae212ce836')


def save(file_name, file_data):
    formatted = json.dumps(file_data, indent=4, sort_keys=False)
    file = open('./dump/%s.json' % file_name, 'w')
    file.write(formatted)
    file.close()


def task_start(task_number):
    name = 'task-%02d-start' % (task_number)
    location = 'task/%d/start' % (task_number)
    resp = requests.post(url=url + location, params=params)
    data = json.loads(resp.text)
    save(name, data)


def task_get(task_number):
    name = 'task-%02d-get' % (task_number)
    location = 'task/%d' % (task_number)
    resp = requests.get(url=url + location, params=params)
    data = json.loads(resp.text)
    save(name, data)
    return data


def task_post(task_number, endpoint):
    name = 'task-%02d-post' % (task_number)
    resp = requests.post(url=url + endpoint, params=params)
    data = json.loads(resp.text)
    save(name, data)


def task_finish(task_number):
    name = 'task-%02d-finish' % (task_number)
    location = 'task/finish'
    resp = requests.post(url=url + location, params=params)
    data = json.loads(resp.text)
    save(name, data)


def twget(name, endpoint, saving=False):
    resp = requests.get(url=url + endpoint, params=params)
    data = json.loads(resp.text)
    if saving:
        save(name, data)
    return data


def twpost(name, endpoint, saving=True):
    resp = requests.post(url=url + endpoint, params=params)
    try:
        data = json.loads(resp.text)
        if saving:
            save(name, data)
        return data

    except:
        data = {'result': resp.text}
        return data


def twput(endpoint):
    resp = requests.put(url=url + endpoint, params=params)
    try:
        data = json.loads(resp.text)
        return data

    except:
        data = {'result': resp.text}
        return data


def twdelete(endpoint):
    resp = requests.delete(url=url + endpoint, params=params)
    try:
        data = json.loads(resp.text)
        return data

    except:
        data = {'result': resp.text}
        return data
