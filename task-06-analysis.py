from imports import *
import scipy.stats as stats
import pandas as pd
import re


def get_ip_mask(ip):
    return '.'.join(ip.split('.')[:-1])


def apply(series, f):
    return pd.Series(map(f, list(series)))


def get_pvalue(name, data):
    data[name] = data[name].astype('category')
    data[name] = data[name].cat.codes / 100.0

    result_fraud = data[data.fraud == True]
    result_normal = data[data.fraud == False]
    statistics = stats.ttest_ind(a=result_fraud[name], b=result_normal[name], equal_var=False)

    return {'feature': name, 'pvalue': statistics[1]}


def get_name(email):
    name = email.split('@')[0].lower()
    name = re.sub(r'-|_|\.', ' ', name)
    return name


def get_email_domain(email):
    return email.split('@')[1]


def get_surname(email):
    name = email.split('@')[0].lower()
    surname = re.sub(r'-|_|\.', ' ', name).split(' ')[-1:][0]
    return surname


def get_fullname(email):
    return get_name(email) + ' ' + get_surname(email)


def display_features():
    # load all existing features
    data = pd.DataFrame(twget('task-06-history', 'payment/history', saving=False))

    # invent new features
    data['ipmask'] = apply(data['ip'], get_ip_mask)
    data['emailDomain'] = apply(data['email'], get_email_domain)
    data['currencyPair'] = data['sourceCurrency'] + data['targetCurrency']
    data['senderFirstName'] = apply(data['email'], get_name)
    data['senderSurname'] = apply(data['email'], get_surname)
    data['senderFullname'] = apply(data['email'], get_fullname)

    # load feature list
    features = [name for name in list(data.columns) if name not in ['fraud', 'amount', 'created']]

    # calculate and print all pvalues
    pvalue_list = list(map(lambda x: get_pvalue(x, pd.DataFrame(data)), features))
    pvalue_frame = pd.DataFrame(pvalue_list).sort_values(by=['pvalue'])
    print(pvalue_frame)


if __name__ == "__main__":
    task_get(6)
    # task_start(6)
    display_features()
