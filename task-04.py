from imports import *
import numpy as np
from itertools import permutations

'''

Task 4

As a next step, let's try to make a few things right in this world. 
Let's catch the banks and currency exchange companies pulling dirty tricks on consumers. 
Let's calculate by how much they are cheating on you by adding hidden fees. 
You have access to a set of quotes offered by different companies. 
You also have access to the real mid-market rate.Catch the cheaters. 
Calculate how big are the hidden fees.

        "GET /currency - get all currencies supported by the companies who are making offers."
        
        "GET /company - get all companies that are making offers",
        
        "GET /quote/<amount>/<sourceCurrency>/<targetCurrency> 
            - get all quotes offered by various banks to transfer a specific amount of money."
        
        "GET /rate/midMarket/<sourceCurrency>/<targetCurrency> 
            - get the real mid-market rate for a currency pair"

        POST /hiddenFee/forCompany/<companyName>/<sourceAmount>/<sourceCurrency>/<targetCurrency>/<hiddenFeePercentage> 

        -   save the percentage that a certain company is cheating their customers on. 
            To calculate hiddenFeePercentage, calculate the difference 
            between what a recipient should get if the company used a fair mid-market rate, 
            and how much more money that would be worth in the source currency. 
            You need to post answers for the amounts of 100, 1000, and 10000 in the source currency. 
            You need to get at least one rate right to move on to the next task. 
            Note that we only care about the number without decimal places 
            (i.e. if the hidden fee is 3%,you should send 3.0)

'''


def load_graph():
    graph = {}
    amounts = [100, 1000, 10000]
    curr = twget('currency', 'currency')
    curr_pairs = list(permutations(curr, 2))
    companies = twget('companies', 'company')

    graph['amounts'] = amounts
    graph['curr'] = curr
    graph['curr_pairs'] = curr_pairs
    graph['companies'] = companies

    def load_quote(amount, curr_pair):
        src, trg = curr_pair
        return twget('quote', 'quote/%d/%s/%s' % (amount, src, trg))

    def load_all_quotes():
        sub_graph = {}
        for a in amounts:
            sub_graph[a] = {}
            for p in curr_pairs:
                res = load_quote(a, p)
                sub_graph[a][p] = res
                src, trg = p
                print('Loading quotes for %s %s%s' % (a, src, trg))
        return sub_graph

    def load_all_market_rates():
        sub_graph = {}
        for p in curr_pairs:
            src, trg = p
            sub_graph[p] = twget('market', 'rate/midMarket/%s/%s' % (src, trg))
            print('Loading market rate for %s%s' % (src, trg))
        return sub_graph

    graph['market_rates'] = load_all_market_rates()
    graph['quotes'] = load_all_quotes()

    return graph


def get_hidden_fee(company, amount, curr_pair, graph):
    src, trg = curr_pair
    qt = graph['quotes'][amount][curr_pair][company]
    market_rates = graph['market_rates']

    if src == 'GBP':
        rate_market_gbp = 1.0
    else:
        rate_market_gbp = market_rates[('GBP', src)]['rate']

    rate_market = market_rates[(src, trg)]['rate']

    #
    rate_market_inverse = market_rates[(trg, src)]['rate']

    fee_gbp = qt['feeInGbp']
    fee_source = fee_gbp * rate_market_gbp
    commission = qt['commissionPercentage'] / 100.0 * amount

    you_pay = amount + fee_source + commission
    you_pay_ = qt['youPay']

    assert round(you_pay, 4) == round(you_pay_, 4), 'Houston we have a problem!'

    #
    trg_offer = qt['targetValue']

    #
    trg_market = amount * rate_market

    hidden = ((trg_market - trg_offer) * rate_market_inverse) / amount

    return round(hidden * 100.0, 0)


def process_all():
    data = load_graph()
    # np.save('dump/task-04-graph.npy', data)
    # data = np.load('dump/task-04-graph.npy').item()

    companies = data['companies']['companies'] \
        .replace('[', '') \
        .replace(']', '') \
        .replace(',', '') \
        .split(' ')

    amounts = data['amounts']
    curr_pairs = data['curr_pairs']

    results = []

    for company in companies:
        for amount in amounts:
            for curr_pair in curr_pairs:
                src, trg = curr_pair
                h = get_hidden_fee(company, amount, curr_pair, data)

                # endpoint
                # /hiddenFee/forCompany/<companyName>/<sourceAmount>/<sourceCurrency>/<targetCurrency>/<hiddenFeePercentage>
                endpoint = '/hiddenFee/forCompany/%s/%s/%s/%s/%s' % \
                           (company,
                            amount,
                            src,
                            trg,
                            h)

                result = twpost('task-04-post', endpoint=endpoint, saving=False)
                print(endpoint, result)
                results.append(result)

    save('task-04-post', results)


if __name__ == "__main__":
    # task_get(4)
    # task_start(4)
    process_all()
    # task_finish(4)
