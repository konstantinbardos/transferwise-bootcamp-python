import json
import requests

url = 'http://54.229.242.6/'
params = dict(token='36db9a2e40f3f1754fa7baae212ce836')


def save(file_name, file_data):

    formatted = json.dumps(file_data, indent=4, sort_keys=False)
    file = open('./dump/%s.json' % file_name, 'w')
    file.write(formatted)
    file.close()


# initial task
if False:
    name = 'ex00'
    resp = requests.get(url=url, params=params)
    data = json.loads(resp.text)
    save(name, data)


# task 01
if True:
    name = 'task01-get'
    location = '/task/1'
    resp = requests.get(url=url+location, params=params)
    data = json.loads(resp.text)
    save(name, data)

    name = 'task01-get-name'
    location = '/name'
    resp = requests.get(url=url+location, params=params)
    data = json.loads(resp.text)
    save(name, data)
